package ab3.impl.DippoldMitterer;

import ab3.AB3;
import ab3.BNode;
import ab3.BTreeMap;

import java.util.ArrayList;
import java.util.List;

public class AB3Impl implements AB3 {

    @Override
    public BTreeMap newBTreeInstance() {

	    return new BTreeMapImpl();
    }

    private class BTreeMapImpl implements BTreeMap{
        private int minDegree = -1;

        private class MyBNode extends BNode{

            /**
             * @return true, falls Node ein Blatt ist. Sonst false.
             */
            private boolean isLeaf(){
                return getChildren().size() == 0;
            }

            /**
             * @return true, falls Node voll ist. Sonst false.
             */
            private boolean isFull(){
                return getKeyValuePairs().size() == 2*minDegree-1;
            }

            private void splitChild(MyBNode child){
                int middleIndex = child.getKeyValuePairs().size()/2;
                KeyValuePair middleKVP = child.getKeyValuePairs().get(middleIndex);
                addKVP(middleKVP);

                List<BNode> oldChildren = child.getChildren();
                List<BNode> leftChildren = new ArrayList<BNode>();
                List<BNode> rightChildren = new ArrayList<BNode>();
                for(int i=0; i<oldChildren.size(); i++){
                    if(i <= middleIndex) leftChildren.add(oldChildren.get(i));
                    if(i > middleIndex) rightChildren.add(oldChildren.get(i));
                }
                //TODO: add leftChildren and rightChildren to this nodes children separately.
            }

            /**
             * Fügt neues KVP in die Node ein und behält Sortierung bei.
             *
             * @param kvp Hinzuzufügendes KVP
             * @throws IllegalStateException falls kein Platz
             */
            private void addKVP(KeyValuePair kvp) throws IllegalStateException {
                if(isFull()) throw new IllegalStateException();

                List<KeyValuePair> newKVPs = getKeyValuePairs();
                int index = 0;
                while (newKVPs.get(index).getKey() > kvp.getKey()) index++;
                newKVPs.add(index, kvp);
                setKeyValuePairs(newKVPs);
            }

            /**
             * Versucht, das übergebene KeyValuePair aufzunehmen.
             *
             * @param kv Aufzunehmendes KeyValuePair
             *
             * @return null, valls die Aufnahme erfolgreich war,
             * @return das in den Parent einzusetzende KeyValuePair falls nicht.
             */
            private KeyValuePair insert(KeyValuePair kv){
                //TODO
            }
        }


        /**
         * Setzt den Minimalgrad des Baumes. Diese Methode muss als erstes und darf kein
         * weiteres Mal aufgerufen werden, solange clear nicht aufgerufen wird.
         *
         * @param t Minimalgrad
         * @throws IllegalStateException    falls die Methode ein zweites Mal aufgerufen wird, ohne dass
         *                                  clear aufgerufen wurde.
         * @throws IllegalArgumentException Falls t ungültigen ist (t<2)
         */
        @Override
        public void setMinDegree(int t) throws IllegalStateException, IllegalArgumentException {

            if(minDegree != -1) throw new IllegalStateException();
            if(t < 2) throw new IllegalArgumentException();

            minDegree = t;
        }

        /**
         * Fügt ein Werte-Paar in den Baum ein. Duplikate (keys) sind im Baum nicht erlaubt.
         *
         * @param key   einzufügender Schlüssel
         * @param value einzufügender Wert
         * @return false, wenn der Schlüssel bereits vorhanden war, andernfalls true
         * @throws IllegalStateException Falls setMinDegree noch nicht aufgerufen wurde
         */
        @Override
        public boolean put(int key, String value) throws IllegalStateException, IllegalArgumentException {

            if(minDegree == -1) throw new IllegalStateException();
            //Don't know when to throw new IllegalArgumentException();


            return false;
        }

        /**
         * Löscht einen Wert aus dem Baum
         *
         * @param key zu löschender Schlüssel
         * @return true, falls der Schlüssel gelöscht wurde. Sonst false.
         * @throws IllegalStateException Falls setMinDegree noch nicht aufgerufen wurde
         */
        @Override
        public boolean delete(int key) throws IllegalStateException, IllegalArgumentException {
            return false;
        }

        /**
         * Prüft, ob ein Schlüsse bereits im Baum vorhanden ist und gibt den dazugehörigen Wert zurück
         *
         * @param key
         * @return der gespeicherte Wert, wenn der Schlüssel vorhanden ist. Sonst null
         * @throws IllegalStateException Falls setMinDegree noch nicht aufgerufen wurde
         */
        @Override
        public String contains(int key) throws IllegalStateException {
            return null;
        }

        /**
         * Liefert den Root-Knoten des Baumes.
         *
         * @return Root-Knoten
         * @throws IllegalStateException Falls setMinDegree noch nicht aufgerufen wurde
         */
        @Override
        public BNode getRoot() throws IllegalStateException {
            return null;
        }

        /**
         * Gibt die Schlüssel des Baumes (sortiert) als Liste zurück
         *
         * @return sortierte Liste der Schlüssel
         * @throws IllegalStateException Falls setMinDegree noch nicht aufgerufen wurde
         */
        @Override
        public List<Integer> getKeys() throws IllegalStateException {
            return null;
        }

        /**
         * Gibt die Werte des Baumes (sortiert nach den Keys) als Liste zurück
         *
         * @return Liste der Werte
         * @throws IllegalStateException Falls setMinDegree noch nicht aufgerufen wurde
         */
        @Override
        public List<String> getValues() throws IllegalStateException {
            return null;
        }

        /**
         * Setzt den Baum zurück. setMinDegree muss wieder aufgerufen werden
         */
        @Override
        public void clear() {

        }

        /**
         * Liefert die Anzahl der gespeicherten Schlüssel-Wert-Paare
         */
        @Override
        public int size() {
            return 0;
        }
    }

}